package com.global.solution.permissions.service.converter;

import com.global.solution.permissions.domain.model.Permission;
import com.global.solution.permissions.service.dto.PermissionResponse;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
public class PermissionToPermissionResponseConverter implements Converter<Permission, PermissionResponse> {

    @NonNull
    @Override public PermissionResponse convert(Permission permission) {
        return PermissionResponse.builder()
            .id(permission.getId())
            .externalId(permission.getExternalId())
            .type(permission.getType())
            .build();
    }
}

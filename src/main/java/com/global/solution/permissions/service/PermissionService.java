package com.global.solution.permissions.service;

import com.global.solution.permissions.service.dto.PermissionRequest;
import com.global.solution.permissions.service.dto.PermissionResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface PermissionService {

     Mono<PermissionResponse> save(PermissionRequest permissionRequest, String userId);
     Mono<PermissionResponse> getByExternalId(String externalId, String userId);
     Flux<PermissionResponse> getByType(String type, String userId);
     Mono<Void> deleteByExternalId(String externalId, String userId);
     Mono<Void> deleteByUserId(String userId);

}

package com.global.solution.permissions.service.impl;

import com.global.solution.common.exception.NotFoundException;
import com.global.solution.permissions.domain.model.Permission;
import com.global.solution.permissions.domain.repository.PermissionRepository;
import com.global.solution.permissions.service.PermissionService;
import com.global.solution.permissions.service.converter.PermissionRequestToPermissionConverter;
import com.global.solution.permissions.service.converter.PermissionToPermissionResponseConverter;
import com.global.solution.permissions.service.dto.PermissionRequest;
import com.global.solution.permissions.service.dto.PermissionResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class PermissionServiceImpl implements PermissionService {

    private final PermissionRepository permissionRepository;
    private final PermissionRequestToPermissionConverter permissionRequestToPermissionConverter;
    private final PermissionToPermissionResponseConverter permissionToPermissionResponseConverter;

    public PermissionServiceImpl(
        PermissionRepository permissionRepository,
        PermissionRequestToPermissionConverter permissionRequestToPermissionConverter,
        PermissionToPermissionResponseConverter permissionToPermissionResponseConverter
    ) {
        this.permissionRepository = permissionRepository;
        this.permissionRequestToPermissionConverter = permissionRequestToPermissionConverter;
        this.permissionToPermissionResponseConverter = permissionToPermissionResponseConverter;
    }

    @Override public Mono<PermissionResponse> save(PermissionRequest permissionRequest, String userId) {
        log.info("save - Saving permission for user id {} , externalId {} and type {} ", userId,
            permissionRequest.getExternalId(), permissionRequest.getType()
        );

        Permission permission = permissionRequestToPermissionConverter.convert(permissionRequest);
        permission.setUserId(userId);
        return permissionRepository.save(permission)
            .map(permissionToPermissionResponseConverter::convert);

    }

    @Override public Mono<PermissionResponse> getByExternalId(String externalId, String userId) {
        log.info("getByExternalId - Getting permission for user id {} and externalId {} ", userId, externalId);

        return permissionRepository.findByExternalIdAndUserId(externalId, userId)
            .switchIfEmpty(Mono.error(new NotFoundException("Permission not found")))
            .map(permissionToPermissionResponseConverter::convert);
    }

    @Override public Flux<PermissionResponse> getByType(String type, String userId) {
        log.info("getByType - Getting permission for user id {} and type {} ", userId, type);

        return permissionRepository.findByTypeAndUserId(type, userId)
            .switchIfEmpty(Mono.error(new NotFoundException("Permissions not found")))
            .map(permissionToPermissionResponseConverter::convert);
    }

    @Override public Mono<Void> deleteByExternalId(String externalId, String userId) {
        log.info("deleteByExternalId - Deleting permission for user id {} and external id {} ", userId, externalId);

        return permissionRepository.deleteByExternalIdAndUserId(externalId, userId);
    }

    @Override public Mono<Void> deleteByUserId(String userId) {
        log.info("deleteByExternalId - Deleting permission for user id {} ", userId);

        return permissionRepository.deleteByUserId(userId);
    }

}

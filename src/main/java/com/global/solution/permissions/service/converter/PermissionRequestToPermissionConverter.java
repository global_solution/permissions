package com.global.solution.permissions.service.converter;

import com.global.solution.permissions.domain.model.Permission;
import com.global.solution.permissions.service.dto.PermissionRequest;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
public class PermissionRequestToPermissionConverter implements Converter<PermissionRequest, Permission> {

    @NonNull
    @Override public Permission convert(PermissionRequest permissionRequest) {
        return Permission.builder()
            .externalId(permissionRequest.getExternalId())
            .type(permissionRequest.getType())
            .build();
    }
}

package com.global.solution.permissions.router.handler;

import com.global.solution.common.exception.BadRequestException;
import com.global.solution.common.exception.BusinessWebException;
import com.global.solution.common.web.error.ErrorResponseBuilder;
import com.global.solution.permissions.service.PermissionService;
import com.global.solution.permissions.service.dto.PermissionRequest;
import com.global.solution.permissions.service.dto.PermissionResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class PermissionHandler {
    private static final String USER_ID = "userId";
    private static final String EXTERNAL_ID = "externalId";
    private static final String TYPE = "type";

    private final PermissionService permissionService;

    public PermissionHandler(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    public Mono<ServerResponse> save(ServerRequest serverRequest) {
        return serverRequest.principal().zipWith(serverRequest.bodyToMono(PermissionRequest.class))
            .flatMap(tuple -> permissionService.save(tuple.getT2(), tuple.getT1().getName()))
            .flatMap(response ->
                ServerResponse
                    .status(HttpStatus.CREATED)
                    .body(Mono.just(response), PermissionResponse.class))
            .onErrorResume(
                RuntimeException.class,
                ErrorResponseBuilder::buildErrorResponse
            );
    }

    public Mono<ServerResponse> getByExternalIdentifier(ServerRequest serverRequest) {
        return serverRequest.queryParam(EXTERNAL_ID)
            .map(externalId ->
                serverRequest.principal()
                    .flatMap(user -> permissionService.getByExternalId(externalId, user.getName()))
                    .flatMap(response -> ServerResponse.ok().body(Mono.just(response), PermissionResponse.class))
                    .onErrorResume(
                        BusinessWebException.class,
                        ErrorResponseBuilder::buildErrorResponse

                    )
            ).orElseGet(() -> ErrorResponseBuilder.buildErrorResponse(new BadRequestException("Invalid query " +
                "parameter")));

    }

    public Mono<ServerResponse> getByType(ServerRequest serverRequest) {
        return serverRequest.queryParam(TYPE)
            .map(type -> {
                Flux<PermissionResponse> permissions = serverRequest.principal()
                    .flatMapMany(user -> permissionService.getByType(type, user.getName()));

                return ServerResponse
                    .ok()
                    .body(permissions, PermissionResponse.class)
                    .onErrorResume(
                        RuntimeException.class,
                        ErrorResponseBuilder::buildErrorResponse
                    );
            }).orElseGet(() -> ErrorResponseBuilder.buildErrorResponse(new BadRequestException("Invalid query " +
                "parameter")));
    }

    public Mono<ServerResponse> deleteByExternalId(ServerRequest serverRequest) {
        return serverRequest.queryParam(EXTERNAL_ID)
            .map(externalId -> serverRequest.principal()
                .flatMap(user -> permissionService.deleteByExternalId(externalId, user.getName()))
                .flatMap(voidResponse -> ServerResponse.noContent().build())
                .onErrorResume(
                    RuntimeException.class,
                    ErrorResponseBuilder::buildErrorResponse
                )
            ).orElseGet(() -> ErrorResponseBuilder.buildErrorResponse(new BadRequestException("Invalid query " +
                "parameter")));
    }

    public Mono<ServerResponse> deleteByUserId(ServerRequest serverRequest) {
        return serverRequest.queryParam(USER_ID)
            .map(userId -> permissionService.deleteByUserId(userId)
                .flatMap(voidResponse -> ServerResponse.noContent().build())
                .onErrorResume(
                    RuntimeException.class,
                    ErrorResponseBuilder::buildErrorResponse
                )
            ).orElseGet(() -> ErrorResponseBuilder.buildErrorResponse(new BadRequestException("Invalid query " +
                "parameter")));
    }
}

package com.global.solution.permissions.router;

import com.global.solution.permissions.router.handler.PermissionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
public class PermissionRouter {

    @Bean
    public RouterFunction<ServerResponse> routerPermissionFunction(PermissionHandler permissionHandler) {
        return RouterFunctions.route(RequestPredicates.POST("/permissions"), permissionHandler::save)
            .andRoute(RequestPredicates.GET("/permissions").and(RequestPredicates.queryParam("externalId"
                , p -> StringUtils.hasText("externalId"))),
                permissionHandler::getByExternalIdentifier)
            .andRoute(RequestPredicates.GET("/permissions").and(RequestPredicates.queryParam("type"
                , p -> StringUtils.hasText("type"))), permissionHandler::getByType)
            .andRoute(RequestPredicates.DELETE("/permissions/{externalIdentifier}"),
                permissionHandler::deleteByExternalId)
            .andRoute(RequestPredicates.DELETE("/permissions/{userId}"), permissionHandler::deleteByUserId);
    }

}

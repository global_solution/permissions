package com.global.solution.permissions.config;

import com.global.solution.common.security.AuthenticationManager;
import com.global.solution.common.security.SecurityConfig;
import com.global.solution.common.security.SecurityContextRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.server.SecurityWebFilterChain;
import reactor.core.publisher.Mono;

@Configuration
public class SecurityConfiguration {

    @Bean
    public SecurityConfig securityConfig() {
        return new SecurityConfig(authenticationManager(), securityContextRepository());
    }

    @Bean
    public AuthenticationManager authenticationManager() {
        return new AuthenticationManager();
    }

    @Bean
    public SecurityContextRepository securityContextRepository() {
        return new SecurityContextRepository(authenticationManager());
    }

    @Bean
    SecurityWebFilterChain springWebFilterChain(ServerHttpSecurity http, AuthenticationManager authenticationManager, SecurityContextRepository securityContextRepository) {
        return http
                .cors()
                .disable()
                .exceptionHandling()
                .authenticationEntryPoint((swe, e) -> Mono.fromRunnable(() -> swe.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED)))
                .accessDeniedHandler((swe, e) -> Mono.fromRunnable(() -> swe.getResponse().setStatusCode(HttpStatus.FORBIDDEN)))
                .and()
                .csrf()
                .disable()
                .authenticationManager(authenticationManager)
                .securityContextRepository(securityContextRepository)
                .authorizeExchange().pathMatchers("/register", "/confirm", "/login").permitAll()
                .pathMatchers(HttpMethod.OPTIONS)
                .permitAll().anyExchange().authenticated().and().build();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}

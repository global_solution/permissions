package com.global.solution.permissions.domain.repository;

import com.global.solution.permissions.domain.model.Permission;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface PermissionRepository extends ReactiveMongoRepository<Permission, String> {

    Mono<Permission> findByExternalIdAndUserId(String externalId, String userId);
    Flux<Permission> findByTypeAndUserId(String type, String userId);
    Mono<Void> deleteByExternalIdAndUserId(String externalId, String userId);
    Mono<Void> deleteByUserId(String userId);

}

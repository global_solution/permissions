FROM openjdk:11-jdk-slim
VOLUME /tmp
WORKDIR /app
COPY /target/permissions-1.0.0.jar /app
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","permissions-1.0.0.jar"]
